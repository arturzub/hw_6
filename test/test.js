describe("spesialMassiveMirror",function (){
    it('return massive ', function () {
        assert.deepEqual(spesialMassiveMirror([1,2]),[2,1]);
    });

    it('return "некорректные данные" when argument null ', function () {
        const actual = spesialMassiveMirror(null);

        const expected = "некорректные данные";
        assert.deepEqual(actual,expected);
    });
})


