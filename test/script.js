function spesialMassiveMirror(arr) {
    if (!Array.isArray(arr)){
        return "некорректные данные";
    }
    var len = arr.length;
    var half = Math.floor(len /2);
    var temp = null;

    for (var i = 0; i < half; i++){
        temp = arr[i];
        arr[i]=arr[len - half + i];
        arr[len - half + i] = temp;
    }
    return arr;
}