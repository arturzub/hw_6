let fromDisplay = document.getElementById('display');
let ac = document.querySelector('.calculator__ac');
let buttons =  document.querySelectorAll('.calculator__button');
let operations =  document.querySelectorAll('.calculator__operation');
let equallyButton = document.querySelector('.calculator__equally');

ac.addEventListener('click', clear);
equallyButton.addEventListener('click', equally);

function debug(x) {
    console.log(x);
}

function display (output, inside) {
    fromDisplay.innerText = fromDisplay.innerText === '0' ? '' : fromDisplay.innerText;
    if (!inside) {
        fromDisplay.innerText += output;
    } else {
        fromDisplay.innerText = output;
    }

}

buttons.forEach(function (button) {
    button.addEventListener('click', function (e) {
        buttonPress(button);
    });
});

operations.forEach(function (operation) {
    operation.addEventListener('click', function (e) {
        operationPress(operation);
    });
});

function buttonPress(number) {
    display(number.getAttribute('value'));
}

function operationPress(operation) {
    display(operation.innerText);
}

function clear() {
    display(0,true)
}

function equally() {
    let exp = fromDisplay.innerText;
    if (exp) {
        display(eval(exp), 1);
        document.getElementById('memory').innerText = exp;
    }
}